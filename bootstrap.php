<?php
use R3C\Wordpress\EventDispatcher\EventDispatcher;

EventDispatcher::addListener(function() {
    add_action( 'wp_enqueue_scripts', 'r3cWordpressJsStartScripts' );
    add_action( 'admin_enqueue_scripts', 'r3cWordpressJsStartScripts' );
});

function r3cWordpressJsStartScripts() {
    $url = str_replace('/wp', '/', get_site_url());
    $js = $url . 'wp-content/plugins/wordpress-js-start/js/';

    wp_register_script('r3c-wordpress-js-start', $js . 'code.min.js', array(), '1.0');
    //Injeta a URL base do site
    wp_localize_script('r3c-wordpress-js-start', 'siteInfo', ['baseUrl' => $url]);
    wp_enqueue_script('r3c-wordpress-js-start');
}

?>